import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import proto.ZodiacServiceGrpc;
import proto.Zodiac;

import java.util.Scanner;

public class MainClass {

    public static void main(String[] args) {
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();
        ZodiacServiceGrpc.ZodiacServiceStub zodiacServiceStub = ZodiacServiceGrpc.newStub(managedChannel);

        System.out.println("Please insert your birth date: ");

        boolean isConnected = true;
        while (isConnected) {
            Scanner scan = new Scanner(System.in);

            String date = scan.next();
            ValidateData valid = new ValidateData();

            if (valid.validate(date) == true) {
                zodiacServiceStub.getDate(Zodiac.BirthDateRequest.newBuilder().setDateOfBirth(date).build(), new StreamObserver<Zodiac.ZodiacSignResponse>() {
                    @Override
                    public void onNext(Zodiac.ZodiacSignResponse zodiacSignResponse) {
                        System.out.println(zodiacSignResponse);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        System.out.println(throwable.getMessage());
                    }

                    @Override
                    public void onCompleted() {
                        System.out.println("Please insert your birth date: ");
                    }
                });

            } else {
                System.out.println("The date is invalid!" + " Please Enter a new one in the correct format.");
                System.out.println("\n");
            }

        }
        managedChannel.shutdown();
    }

}
