package proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 *
 */
@javax.annotation.Generated(
        value = "by gRPC proto compiler (version 1.15.0)",
        comments = "Source: fall.proto")
public final class FallServiceGrpc {

    private FallServiceGrpc() {
    }

    public static final String SERVICE_NAME = "FallService";

    // Static method descriptors that strictly reflect the proto.
    private static volatile io.grpc.MethodDescriptor<proto.Fall.BirthDateRequest,
            proto.Fall.ZodiacSignResponse> getGetDateMethod;

    @io.grpc.stub.annotations.RpcMethod(
            fullMethodName = SERVICE_NAME + '/' + "getDate",
            requestType = proto.Fall.BirthDateRequest.class,
            responseType = proto.Fall.ZodiacSignResponse.class,
            methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
    public static io.grpc.MethodDescriptor<proto.Fall.BirthDateRequest,
            proto.Fall.ZodiacSignResponse> getGetDateMethod() {
        io.grpc.MethodDescriptor<proto.Fall.BirthDateRequest, proto.Fall.ZodiacSignResponse> getGetDateMethod;
        if ((getGetDateMethod = FallServiceGrpc.getGetDateMethod) == null) {
            synchronized (FallServiceGrpc.class) {
                if ((getGetDateMethod = FallServiceGrpc.getGetDateMethod) == null) {
                    FallServiceGrpc.getGetDateMethod = getGetDateMethod =
                            io.grpc.MethodDescriptor.<proto.Fall.BirthDateRequest, proto.Fall.ZodiacSignResponse>newBuilder()
                                    .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
                                    .setFullMethodName(generateFullMethodName(
                                            "FallService", "getDate"))
                                    .setSampledToLocalTracing(true)
                                    .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                            proto.Fall.BirthDateRequest.getDefaultInstance()))
                                    .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                            proto.Fall.ZodiacSignResponse.getDefaultInstance()))
                                    .setSchemaDescriptor(new FallServiceMethodDescriptorSupplier("getDate"))
                                    .build();
                }
            }
        }
        return getGetDateMethod;
    }

    /**
     * Creates a new async stub that supports all call types for the service
     */
    public static FallServiceStub newStub(io.grpc.Channel channel) {
        return new FallServiceStub(channel);
    }

    /**
     * Creates a new blocking-style stub that supports unary and streaming output calls on the service
     */
    public static FallServiceBlockingStub newBlockingStub(
            io.grpc.Channel channel) {
        return new FallServiceBlockingStub(channel);
    }

    /**
     * Creates a new ListenableFuture-style stub that supports unary calls on the service
     */
    public static FallServiceFutureStub newFutureStub(
            io.grpc.Channel channel) {
        return new FallServiceFutureStub(channel);
    }

    /**
     *
     */
    public static abstract class FallServiceImplBase implements io.grpc.BindableService {

        /**
         *
         */
        public void getDate(proto.Fall.BirthDateRequest request,
                            io.grpc.stub.StreamObserver<proto.Fall.ZodiacSignResponse> responseObserver) {
            asyncUnimplementedUnaryCall(getGetDateMethod(), responseObserver);
        }

        @java.lang.Override
        public final io.grpc.ServerServiceDefinition bindService() {
            return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
                    .addMethod(
                            getGetDateMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            proto.Fall.BirthDateRequest,
                                            proto.Fall.ZodiacSignResponse>(
                                            this, METHODID_GET_DATE)))
                    .build();
        }
    }

    /**
     *
     */
    public static final class FallServiceStub extends io.grpc.stub.AbstractStub<FallServiceStub> {
        private FallServiceStub(io.grpc.Channel channel) {
            super(channel);
        }

        private FallServiceStub(io.grpc.Channel channel,
                                io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
        }

        @java.lang.Override
        protected FallServiceStub build(io.grpc.Channel channel,
                                        io.grpc.CallOptions callOptions) {
            return new FallServiceStub(channel, callOptions);
        }

        /**
         *
         */
        public void getDate(proto.Fall.BirthDateRequest request,
                            io.grpc.stub.StreamObserver<proto.Fall.ZodiacSignResponse> responseObserver) {
            asyncUnaryCall(
                    getChannel().newCall(getGetDateMethod(), getCallOptions()), request, responseObserver);
        }
    }

    /**
     *
     */
    public static final class FallServiceBlockingStub extends io.grpc.stub.AbstractStub<FallServiceBlockingStub> {
        private FallServiceBlockingStub(io.grpc.Channel channel) {
            super(channel);
        }

        private FallServiceBlockingStub(io.grpc.Channel channel,
                                        io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
        }

        @java.lang.Override
        protected FallServiceBlockingStub build(io.grpc.Channel channel,
                                                io.grpc.CallOptions callOptions) {
            return new FallServiceBlockingStub(channel, callOptions);
        }

        /**
         *
         */
        public proto.Fall.ZodiacSignResponse getDate(proto.Fall.BirthDateRequest request) {
            return blockingUnaryCall(
                    getChannel(), getGetDateMethod(), getCallOptions(), request);
        }
    }

    /**
     *
     */
    public static final class FallServiceFutureStub extends io.grpc.stub.AbstractStub<FallServiceFutureStub> {
        private FallServiceFutureStub(io.grpc.Channel channel) {
            super(channel);
        }

        private FallServiceFutureStub(io.grpc.Channel channel,
                                      io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
        }

        @java.lang.Override
        protected FallServiceFutureStub build(io.grpc.Channel channel,
                                              io.grpc.CallOptions callOptions) {
            return new FallServiceFutureStub(channel, callOptions);
        }

        /**
         *
         */
        public com.google.common.util.concurrent.ListenableFuture<proto.Fall.ZodiacSignResponse> getDate(
                proto.Fall.BirthDateRequest request) {
            return futureUnaryCall(
                    getChannel().newCall(getGetDateMethod(), getCallOptions()), request);
        }
    }

    private static final int METHODID_GET_DATE = 0;

    private static final class MethodHandlers<Req, Resp> implements
            io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
        private final FallServiceImplBase serviceImpl;
        private final int methodId;

        MethodHandlers(FallServiceImplBase serviceImpl, int methodId) {
            this.serviceImpl = serviceImpl;
            this.methodId = methodId;
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                case METHODID_GET_DATE:
                    serviceImpl.getDate((proto.Fall.BirthDateRequest) request,
                            (io.grpc.stub.StreamObserver<proto.Fall.ZodiacSignResponse>) responseObserver);
                    break;
                default:
                    throw new AssertionError();
            }
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public io.grpc.stub.StreamObserver<Req> invoke(
                io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                default:
                    throw new AssertionError();
            }
        }
    }

    private static abstract class FallServiceBaseDescriptorSupplier
            implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
        FallServiceBaseDescriptorSupplier() {
        }

        @java.lang.Override
        public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
            return proto.Fall.getDescriptor();
        }

        @java.lang.Override
        public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
            return getFileDescriptor().findServiceByName("FallService");
        }
    }

    private static final class FallServiceFileDescriptorSupplier
            extends FallServiceBaseDescriptorSupplier {
        FallServiceFileDescriptorSupplier() {
        }
    }

    private static final class FallServiceMethodDescriptorSupplier
            extends FallServiceBaseDescriptorSupplier
            implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
        private final String methodName;

        FallServiceMethodDescriptorSupplier(String methodName) {
            this.methodName = methodName;
        }

        @java.lang.Override
        public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
            return getServiceDescriptor().findMethodByName(methodName);
        }
    }

    private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

    public static io.grpc.ServiceDescriptor getServiceDescriptor() {
        io.grpc.ServiceDescriptor result = serviceDescriptor;
        if (result == null) {
            synchronized (FallServiceGrpc.class) {
                result = serviceDescriptor;
                if (result == null) {
                    serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
                            .setSchemaDescriptor(new FallServiceFileDescriptorSupplier())
                            .addMethod(getGetDateMethod())
                            .build();
                }
            }
        }
        return result;
    }
}
