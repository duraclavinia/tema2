import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ValidateData {
    public boolean validate(String dateOfBirth) {
        try {
            DateFormat date = new SimpleDateFormat("MM/dd/yyyy");
            date.setLenient(false);
            date.parse(dateOfBirth);
            return true;
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("Eroare!");
            return false;
        }
    }
}
