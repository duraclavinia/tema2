import io.grpc.Server;
import io.grpc.ServerBuilder;
import service.*;

import java.io.IOException;

public class MainClass {
    public static void main(String[] args) {
        /* *
         * Do not forget to install maven. The grpc stub classes are generated when you run the protoc compiler
         * and it finds a service declaration in your proto file.
         * Do not forget the client must use the same port in order to connect to this server.
         * */

        try {

            Server server = ServerBuilder.forPort(8999).addService( new ZodiacImpl()).addService(new WinterImpl()).addService(new SpringImpl()).addService(new SummerImpl()).addService(new FallImpl()).build();

            server.start();

            System.out.println("Server started at " + server.getPort());
            System.out.println("Hello!");

            server.awaitTermination();
        } catch (IOException e) {
            System.out.println("Error: " + e);
        } catch (InterruptedException e) {
            System.out.println("Error: " + e);
        }
    }
}