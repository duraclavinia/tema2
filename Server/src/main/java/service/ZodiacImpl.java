package service;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import proto.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;


public class ZodiacImpl extends ZodiacServiceGrpc.ZodiacServiceImplBase {

    public static boolean validateDate(String dateOfBirth) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            dateFormat.setLenient(false);
            dateFormat.parse(dateOfBirth);
            return true;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void getDate(Zodiac.BirthDateRequest request, StreamObserver<Zodiac.ZodiacSignResponse> responseObserver) {

        String birthDate = request.getDateOfBirth();

        if (validateDate(birthDate) == false) {
            System.out.println("Error! Enter another date!");

            Status status = Status.INVALID_ARGUMENT.withDescription("Error");
            responseObserver.onError(status.asRuntimeException());

        } else {
            System.out.println("Valid date. :) ");
            ManagedChannel managedChannel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();

            int month = Integer.parseInt(birthDate.split("/")[0]);

            switch (month) {
                case 1:
                case 2:
                case 12:
                    WinterServiceGrpc.WinterServiceBlockingStub winterServiceBlockingStub = WinterServiceGrpc.newBlockingStub(managedChannel);
                    Winter.ZodiacSignResponse winterResponse = winterServiceBlockingStub.getDate(Winter.BirthDateRequest.newBuilder().setDateOfBirth(birthDate).build());
                    Zodiac.ZodiacSignResponse.Builder zodiacResponseWinter = Zodiac.ZodiacSignResponse.newBuilder();
                    zodiacResponseWinter.setZodiacSign(winterResponse.getZodiacSign());
                    responseObserver.onNext(zodiacResponseWinter.build());
                    System.out.println("The zodiac sign is: " + winterResponse.getZodiacSign());
                    System.out.println("\n");
                    break;

                case 3:
                case 4:
                case 5:
                    SpringServiceGrpc.SpringServiceBlockingStub springServiceBlockingStub = SpringServiceGrpc.newBlockingStub(managedChannel);
                    Spring.ZodiacSignResponse springResponse = springServiceBlockingStub.getDate(Spring.BirthDateRequest.newBuilder().setDateOfBirth(birthDate).build());
                    Zodiac.ZodiacSignResponse.Builder zodiacResponseSpring = Zodiac.ZodiacSignResponse.newBuilder();
                    zodiacResponseSpring.setZodiacSign(springResponse.getZodiacSign());
                    responseObserver.onNext(zodiacResponseSpring.build());
                    System.out.println("The zodiac sign is: " + springResponse.getZodiacSign());
                    System.out.println("\n");
                    break;

                case 6:
                case 7:
                case 8:
                    SummerServiceGrpc.SummerServiceBlockingStub summerServiceBlockingStub = SummerServiceGrpc.newBlockingStub(managedChannel);
                    Summer.ZodiacSignResponse summerResponse = summerServiceBlockingStub.getDate(Summer.BirthDateRequest.newBuilder().setDateOfBirth(birthDate).build());
                    Zodiac.ZodiacSignResponse.Builder zodiacResponseSummer = Zodiac.ZodiacSignResponse.newBuilder();
                    zodiacResponseSummer.setZodiacSign(summerResponse.getZodiacSign());
                    responseObserver.onNext(zodiacResponseSummer.build());
                    System.out.println("The zodiac sign is: " + summerResponse.getZodiacSign());
                    System.out.println("\n");
                    break;

                case 9:
                case 10:
                case 11:
                    FallServiceGrpc.FallServiceBlockingStub fallServiceBlockingStub = FallServiceGrpc.newBlockingStub(managedChannel);
                    Fall.ZodiacSignResponse fallResponse = fallServiceBlockingStub.getDate(Fall.BirthDateRequest.newBuilder().setDateOfBirth(birthDate).build());
                    Zodiac.ZodiacSignResponse.Builder zodiacResponseFall = Zodiac.ZodiacSignResponse.newBuilder();
                    zodiacResponseFall.setZodiacSign(fallResponse.getZodiacSign());
                    responseObserver.onNext(zodiacResponseFall.build());
                    System.out.println("The zodiac sign is: " + fallResponse.getZodiacSign());
                    System.out.println("\n");
                    break;
            }
            responseObserver.onCompleted();
            managedChannel.shutdown();

        }

    }
}
