package service;

import io.grpc.stub.StreamObserver;
import proto.Fall;
import proto.FallServiceGrpc;

import java.text.ParseException;
import java.util.ArrayList;

public class FallImpl extends FallServiceGrpc.FallServiceImplBase {
    @Override
    public void getDate(Fall.BirthDateRequest request, StreamObserver<Fall.ZodiacSignResponse> responseObserver) {
        Fall.ZodiacSignResponse.Builder response = Fall.ZodiacSignResponse.newBuilder();

        ZodiacIntervals signs = new ZodiacIntervals();
        ArrayList<EachZodiacSign> zodiacSignArrayList = signs.readTextFile("C:\\Users\\Laptop\\Desktop\\Tema2CNA\\Server\\src\\main\\resources\\fall.txt");
        try {
            String sign = signs.getZodiacSign(request.getDateOfBirth(), zodiacSignArrayList);

            response.setZodiacSign(sign);

            responseObserver.onNext(response.build());
            responseObserver.onCompleted();

        } catch (ParseException e) {
            System.out.println("Eroare!");
            e.printStackTrace();
        }
    }
}
