package service;

import io.grpc.stub.StreamObserver;
import proto.Summer;
import proto.SummerServiceGrpc;


import java.text.ParseException;
import java.util.ArrayList;

public class SummerImpl extends SummerServiceGrpc.SummerServiceImplBase {
    @Override
    public void getDate(Summer.BirthDateRequest request, StreamObserver<Summer.ZodiacSignResponse> responseObserver) {
        Summer.ZodiacSignResponse.Builder response = Summer.ZodiacSignResponse.newBuilder();

        ZodiacIntervals signs = new ZodiacIntervals();
        ArrayList<EachZodiacSign> zodiacSignArrayList = signs.readTextFile("C:\\Users\\Laptop\\Desktop\\Tema2CNA\\Server\\src\\main\\resources\\summer.txt");
        try {
            String sign = signs.getZodiacSign(request.getDateOfBirth(), zodiacSignArrayList);

            response.setZodiacSign(sign);

            responseObserver.onNext(response.build());
            responseObserver.onCompleted();

        } catch (ParseException e) {
            System.out.println("Eroare!");
            e.printStackTrace();
        }
    }
}
