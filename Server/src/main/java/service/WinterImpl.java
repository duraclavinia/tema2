package service;

import io.grpc.stub.StreamObserver;
import proto.Winter;
import proto.WinterServiceGrpc;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;

public class WinterImpl extends WinterServiceGrpc.WinterServiceImplBase {

    @Override
    public void getDate(Winter.BirthDateRequest request, StreamObserver<Winter.ZodiacSignResponse> responseObserver) throws FileNotFoundException {
        Winter.ZodiacSignResponse.Builder response = Winter.ZodiacSignResponse.newBuilder();

        ZodiacIntervals signs = new ZodiacIntervals();
        ArrayList<EachZodiacSign> zodiacSignArrayList = signs.readTextFile("C:\\Users\\Laptop\\Desktop\\Tema2CNA\\Server\\src\\main\\resources\\winter.txt");
        try {
            String sign = signs.getZodiacSign(request.getDateOfBirth(), zodiacSignArrayList);

            response.setZodiacSign(sign);

            responseObserver.onNext(response.build());
            responseObserver.onCompleted();

        } catch (ParseException e) {
            System.out.println("Eroare!");
            e.printStackTrace();
        }

    }
}
