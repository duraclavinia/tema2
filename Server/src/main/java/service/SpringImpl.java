package service;

import io.grpc.stub.StreamObserver;
import proto.Spring;
import proto.SpringServiceGrpc;
import proto.Winter;

import java.text.ParseException;
import java.util.ArrayList;

public class SpringImpl extends SpringServiceGrpc.SpringServiceImplBase {
    @Override
    public void getDate(Spring.BirthDateRequest request, StreamObserver<Spring.ZodiacSignResponse> responseObserver) {
        Spring.ZodiacSignResponse.Builder response = Spring.ZodiacSignResponse.newBuilder();

        ZodiacIntervals signs = new ZodiacIntervals();
        ArrayList<EachZodiacSign> zodiacSignArrayList = signs.readTextFile("C:\\Users\\Laptop\\Desktop\\Tema2CNA\\Server\\src\\main\\resources\\spring.txt");
        try {
            String sign = signs.getZodiacSign(request.getDateOfBirth(), zodiacSignArrayList);

            response.setZodiacSign(sign);

            responseObserver.onNext(response.build());
            responseObserver.onCompleted();

        } catch (ParseException e) {
            System.out.println("Eroare!");
            e.printStackTrace();
        }
    }
}
