package service;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ZodiacIntervals {

    ArrayList<EachZodiacSign> zodiacSignList = new ArrayList<>();
    String zodiacSign, startingDate, endingDate;

    public ArrayList<EachZodiacSign> readTextFile(String fileName) {
        try {
            File file = new File(fileName); //creating the file object
            Scanner scanner = new Scanner(file);
            EachZodiacSign signToRead;
            while (scanner.hasNext()) {
                zodiacSign = scanner.next();
                startingDate = scanner.next();
                endingDate = scanner.next();

                signToRead = new EachZodiacSign(zodiacSign, startingDate, endingDate);
                zodiacSignList.add(signToRead);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return zodiacSignList;
    }

    public String getZodiacSign(String birthOfDate, ArrayList<EachZodiacSign> zodiacs) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("MM/dd");

        for (EachZodiacSign zodiac : zodiacs) {
            Date birthDate = dateFormat.parse(birthOfDate);
            Date starting = dateFormat.parse(zodiac.getStartingDate());
            Date ending = dateFormat.parse(zodiac.getEndingDate());

            Calendar date = Calendar.getInstance();
            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();
            date.setTime(birthDate);
            start.setTime(starting);
            end.setTime(ending);

            if (date.after(start) && date.before(end) || date.equals(start) || date.equals(end)) {
                return zodiac.getZodiacSign();
            }
        }

        return null;
    }
}
