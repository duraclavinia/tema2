package service;

public class EachZodiacSign {

    private String zodiacSign;
    private String startingDate;
    private String endingDate;


    public EachZodiacSign(String zodiacSign, String startingDate, String endingDate) {
        this.zodiacSign = zodiacSign;
        this.startingDate = startingDate;
        this.endingDate = endingDate;
    }

    public String getZodiacSign() {
        return zodiacSign;
    }

    public void setZodiacSign(String zodiacSign) {
        this.zodiacSign = zodiacSign;
    }

    public String getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(String startingDate) {
        this.startingDate = startingDate;
    }

    public String getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(String endingDate) {
        this.endingDate = endingDate;
    }
}
